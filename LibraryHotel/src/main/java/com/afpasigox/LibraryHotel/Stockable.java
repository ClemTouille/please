package com.afpasigox.LibraryHotel;

public abstract class Stockable {
	
	private int numcat;
	private String namecat;
	
	private TypeHotel numTypeHotel;
	private int numhotel;
	private String namehotel;
	private String adrhotel;
	private String Cphotel;
	private String Villehotel;
	
	private int numchambre;
	
	
	
	public Stockable(int numcat,String nomcat) {
		this.numcat=numcat;
		this.namecat=nomcat;
	}
	
	
	
	public Stockable(int numhotel,TypeHotel numtypehotel,String adr,String Cpn,String Ville) {
		this.numhotel=numhotel;
		this.numTypeHotel=numtypehotel;
		this.adrhotel=adr;
		this.Cphotel=Cpn;
		this.Villehotel=Ville;
	}
	
	public Stockable(int numChambre,int numCat,int numHotel) {
		this.numchambre=numChambre;
		this.numcat=numCat;
		this.numhotel=numHotel;
		
	}



	public int getNumcat() {
		return numcat;
	}



	public void setNumcat(int numcat) {
		this.numcat = numcat;
	}



	public String getNamecat() {
		return namecat;
	}



	public void setNamecat(String namecat) {
		this.namecat = namecat;
	}



	public int getNumhotel() {
		return numhotel;
	}



	public void setNumhotel(int numhotel) {
		this.numhotel = numhotel;
	}



	public String getNamehotel() {
		return namehotel;
	}



	public void setNamehotel(String namehotel) {
		this.namehotel = namehotel;
	}



	public String getAdrhotel() {
		return adrhotel;
	}



	public void setAdrhotel(String adrhotel) {
		this.adrhotel = adrhotel;
	}



	public String getCphotel() {
		return Cphotel;
	}



	public void setCphotel(String cphotel) {
		Cphotel = cphotel;
	}



	public String getVillehotel() {
		return Villehotel;
	}



	public void setVillehotel(String villehotel) {
		Villehotel = villehotel;
	}



	public int getNumchambre() {
		return numchambre;
	}



	public void setNumchambre(int numchambre) {
		this.numchambre = numchambre;
	}
	
	

}
