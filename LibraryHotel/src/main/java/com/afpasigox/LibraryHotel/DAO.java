package com.afpasigox.LibraryHotel;



public abstract class DAO {
	
	private Connexion cxion = null;
	
	public DAO(Connexion c) {
		this.cxion=c;
	}
	
	
	protected Connexion getCxion() {
		return this.cxion;
	}
	
	
	

}
