package com.afpasigox.LibraryHotel;

import java.util.HashMap;
import java.util.Map;

public class Collectionneur<T extends Stockable> {
	
	private Map<Integer, T> stock = new HashMap<Integer, T>();
	
	
	public void addItemByNumcat(T pItem) {
		stock.put(pItem.getNumcat(), pItem);
	}
	
	public void addItemByNumType(T pItem) {
		stock.put(pItem.getNumcat(), pItem);
	}
	
	public void addItemByNumHotel(T pItem) {
		stock.put(pItem.getNumhotel(), pItem);
	}
	
	public T getItem(Integer itemId) {
		return stock.get(itemId);
	}
}
