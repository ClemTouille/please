package com.afpasigox.LibraryHotel;

public class DAOFactory {
	
	public static final int DAOCategory 	= 1;
	public static final int DAOHotelType	= 2;
	public static final int DAOHotel 	= 3;
	public static final int DAOChambre	= 4;
	
	private Connexion cxion = null;
	
	
	public DAOFactory(Connexion c) {
		cxion=c;
	}
	
	
	public DAO getDAO(int qui) {
		switch (qui) {
			case DAOCategory : 
				return new DAOCategory(cxion);

			case DAOHotelType : 
				return new DAOHotelType(cxion);
				
			case DAOHotel : 
				return new DAOHotel(cxion);

			case DAOChambre : 
				return new DAOChambre(cxion);
			default :
				return null;
			
		}
	}

}
