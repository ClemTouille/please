package com.afpasigox.LibraryHotel;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

// Cette classe cr�ee une connexion � la BD postgres et la met � dispo 
public class Connexion {
	
	private String address;
	private String name;
	private String password;
	private String driver;
	private Connection c;
	
	public Connexion(String address, String name, String password, String driver) {
		this.address 	= address;
		this.name 		= name;
		this.password 	= password;
		this.driver 	= driver;
		try {
			// On charge le driver
			Class.forName(driver);
			this.c = DriverManager
			        .getConnection(address, name, password);
			c.setAutoCommit(true);
		} catch (SQLException e) {
			e.printStackTrace();
			// Si le driver n'a pas �t� charg�
		} catch (ClassNotFoundException e) {
			// printStackTrace() : lignes rouges d'erreur qu'on a souvent 
			e.printStackTrace();
		}
	}

	public Connection getC() {
		return c;
	}

	public void closeConnection() {
		try {
			this.c.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
}
