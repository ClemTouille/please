package com.afpasigox.LibraryHotel;



import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

//import java.util.logging.Level;
//import java.util.logging.Logger;

public class JournalLog {
	//ATTRIBUTS
	//private static final  Logger LOGGER = Logger.getLogger("utils.JournalLog"); 
	private static final Logger LOGGER = LogManager.getLogger(JournalLog.class);
	public static final  int EASY=0; 
	public static final  int PEPOUZE=1; 
	public static final  int ATTENTION=2; 
	public static final  int CATA=3; 
	public static final int DEBUG = 0;
	public static final int INFO = 1;
	public static final int WARN = 2;
	public static final int ERROR = 3;




	//METHODES
	public static void addMsg(int level, String msg) {

		switch (level) {
		case EASY:
//			LOGGER.setLevel(Level.FINE);
//			LOGGER.fine(msg);
			LOGGER.info(msg);
			break;
		case PEPOUZE:
//			LOGGER.setLevel(Level.INFO);
//			LOGGER.info(msg);
			LOGGER.debug(msg);
			break;
		case ATTENTION:
//			LOGGER.setLevel(Level.SEVERE);
//			LOGGER.severe(msg);
			LOGGER.warn(msg);
			break;
		case CATA:
//			LOGGER.setLevel(Level.WARNING);
//			LOGGER.warning(msg);
			LOGGER.fatal(msg);
			break;

		default:
//			LOGGER.setLevel(Level.SEVERE);
//			LOGGER.severe(msg);
			break;
		}


	}
	public static void addMessage(int lvl, String message) {
		switch(lvl) {
			case DEBUG:
				LOGGER.debug(message);
				break;
			case INFO:
				LOGGER.info(message);
				break;
			case WARN:
				LOGGER.warn(message);
				break;
			case ERROR:
				LOGGER.error(message);
				break;
			default:
				break;
		}
	}


}
